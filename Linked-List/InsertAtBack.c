#include<stdio.h>
#include<stdlib.h>

struct listnode {
	int data;
	struct listnode *next;
};
typedef struct listnode LN;

void insert_at_back(LN **hptr,int d);
void print(LN *hptr);
LN *find_tail(LN *hptr);
int sum(LN *hptr);

int main() {
	LN *head = NULL;
	int temp;
	int i;
	do {
		scanf("%d",&temp);
			if(temp > 0)
				insert_at_back(&head,temp);
	}
	while(temp > 0);	
 	printf("Enter: ");  
 	print(head);
 	printf("\nSUM %d",sum(head));
		return 0; 
}
LN *find_tail(LN *hptr) {
	LN *tail;
		if(hptr == NULL)
			return NULL;
		tail = hptr;
		while(tail->next != NULL) {
			tail = tail->next;
	}
	return tail;
}
void insert_at_back(LN **hptr,int d) {
	LN *add_node,*tail;
	add_node = (LN*)malloc(sizeof(LN));
	add_node->data = d;
	add_node->next = NULL;
	tail = find_tail(*hptr);
	if(tail==NULL)
	*hptr = add_node;
	else
	tail->next = add_node;
}
void print(LN *hptr) {
	while(hptr) {
		printf(" %d ",hptr->data);
		hptr = hptr->next;
	}
}
int sum(LN *hptr) {
	int sum = 0 ; 
	while(hptr) {
		sum += hptr->data;
		hptr = hptr->next;
	}
	return sum;
}
