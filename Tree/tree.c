#include <stdio.h>
#include <stdlib.h>

struct treenode
{
    struct treenode *left;
    struct treenode *right;
    int data;
};

typedef struct treenode TREENODE;
typedef TREENODE *TREE;

void insert_node(TREE *tp, int value);
int is_perfect_tree(TREE t);

int main()
{
    TREE t = NULL;
    int i = 0, tmp;
    do
    {
        printf("N%02d = ", i + 1);
        scanf("%d", &tmp);
        if (tmp <= 0)
        {
            break;
        }
        insert_node(&t, tmp);
        i++;
    } while (tmp > 0);
    printf("= %s\n", is_perfect_tree(t) ? "Yes" : "No");
    return 0;
}
void insert_node(TREE *tp, int value)
{
    if (*tp == NULL)
    {
        *tp = malloc(sizeof(TREENODE));
        (*tp)->data = value;
        (*tp)->left = NULL;
        (*tp)->right = NULL;
    }
    else if (value < (*tp)->data)
        insert_node(&(*tp)->left, value);
    else if (value > (*tp)->data)
        insert_node(&(*tp)->right, value);
    else
        printf("Duplicate node\n");
}
int is_perfect_tree(TREE t)
{
    if (t->left == NULL && t->right == NULL)
        return 1;
    else if (t->left == NULL || t->right == NULL)
        return 0;
}

