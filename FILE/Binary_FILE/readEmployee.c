#include<stdio.h>
#include<stdlib.h>

struct employee {
     char name[128];
     float salary;
};
typedef struct employee Employee;
float total_salary(Employee a[], int len);
int main(){
    int num;
    int i;
    Employee *all;
        all = (Employee*)malloc(num*sizeof(Employee));
    FILE *fp = fopen("employee.bin","rb");
    fread(&num,sizeof(int),1,fp);
    
    printf("Num = %d\n",num);
    
    for(i=0;i<num;i++)
        printf("%d \t %s \t %.2f \n",(i+1),(all+i)->name,(all+i)->salary);
    free(all);
    fclose(fp);

    return 0;
}
float total_salary(Employee all[], int len){
     int i;
     float sum=0;
     for(i=0;i<len;i++)
          sum+=(all+i)->salary;
     return sum;
}
