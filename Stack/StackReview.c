#include<stdio.h>
#include<stdlib.h>
struct stack{
	int *datas;
	int capacity;
	int cursor;
};
struct node{
	int data;
	struct node *next;
};
typedef struct stack Stack;
Stack *stack_init(int capacity); 		// Stack Data Structure Initialize.
void stack_push(Stack *stack,int data); // Push Node into Stack
int stack_pop(Stack *stack); 			// Pop Node out from Stack
int stack_is_empty(Stack *stack); 		// Check is a Empty Stack?
void stack_free(Stack **sptr); 			// Free Stack Memory

int main()
{
	int stk_count; // Stack Node Element
	printf("How Many Stack Element Do You Want ? : ");
	scanf("%d",&stk_count);
	Stack *s = stack_init(stk_count);
	int value[stk_count],i;
	for(i = 0; i< stk_count; i++){
		printf("Element[%d] : ",i+1);
		scanf("%d",&value[i]);
		stack_push(s,value[i]);
	}
    printf("\n");
	printf("IN STACK => ");
	while(!stack_is_empty(s)){ // print all in stack
		printf("|%d| ",stack_pop(s));
	}
	stack_free(&s);
	return 0;
}
Stack *stack_init(int capacity){
	Stack  *s = NULL;
	if(capacity <= 0)
		return NULL;
	s = (Stack*)malloc(sizeof(Stack));
	s->datas = malloc(sizeof(int)*capacity);
	s->capacity = capacity;
	s->cursor = 0;
	return s;
}
void stack_push(Stack *stack,int data){
	if(stack->cursor >= stack->capacity){
		printf("Stack Overflow!");
		return;
	}
	stack->datas[stack->cursor] = data;
	stack->cursor++;
}
int stack_pop(Stack *stack){
	int d = 0;
	if(stack->cursor <= 0){
		printf("Stack Underflow!");
		return 0;
	}
	d = stack->datas[stack->cursor - 1];
	stack->cursor--;
	return d;
}
int stack_is_empty(Stack *stack){
	return (stack == NULL || stack->cursor == 0) ? 1 : 0;
}
void stack_free(Stack **sptr){
	Stack *s = *sptr;
	free(s->datas);
	free(s);
	*sptr = NULL;
}
